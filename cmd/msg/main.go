package main

import (
	"fmt"

	"gitlab.com/vivianext/common/message/pkg/cat"
	"gitlab.com/vivianext/common/message/pkg/dog"
)

func main() {
	fmt.Println(cat.Text())
	fmt.Println(dog.Text())
}
